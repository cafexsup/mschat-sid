#import <UIKit/UIKit.h>
#import <LiveAssist/LiveAssist.h>
#import <JWT/JWT.h>


@interface ViewController : UIViewController

+ (JWTBuilder *)encodePayload:(NSDictionary *)payload;
+ (JWTBuilder *)encodeClaimsSet:(JWTClaimsSet *)claimsSet;
+ (JWTBuilder *)decodeMessage:(NSString *)message;

@property LiveAssistView *assistView;

@end

