#import "ViewController.h"
#import "AppSettings.h"
#import <LiveAssist/LiveAssist.h>
#import <JWT/JWT.h>
@import WebKit;

@interface ViewController ()<UIWebViewDelegate,UITabBarDelegate,UITabBarControllerDelegate,LiveAssistDelegate,WKScriptMessageHandler,WKNavigationDelegate>{
    WKWebView *webView;
    WKWebViewConfiguration *configuration;
    WKUserContentController *controller;
    __weak IBOutlet UIView *webViewContainer;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupWebView];
    [self setupLiveAssist];

}

-(void) setupWebView {
    NSString *url = [AppSettings webSite];
    
//    NSString* javascript = @"function addclickHandler() { var els = document.getElementById('howdoipaymybill'); els.onclick = function() { window.webkit.messageHandlers.observe.postMessage('creditcard'); console.log('message'); } }  ; window.setTimeout(function () { addclickHandler();},3000);";
    
    NSString *jwt = [AppSettings jwt];
    NSLog(@"jwt: %@", jwt);

    configuration = [[WKWebViewConfiguration alloc] init];
    controller = [[WKUserContentController alloc] init];
    [controller addScriptMessageHandler:self name:@"observe"];
    WKUserScript *javascriptFunctions = [[WKUserScript alloc] initWithSource:[self loadJavaScriptFromFileName:@"scriptfunctions"] injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    [controller addUserScript:javascriptFunctions];
    configuration.userContentController = controller;

    CGRect viewframe = webViewContainer.frame;
    webView = [[WKWebView alloc] initWithFrame:viewframe configuration:configuration];
    webView.navigationDelegate = self;

    [webView loadRequest:[[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]]];
    [webViewContainer addSubview:webView];

}

-(void) setupLiveAssist {
    AssistConfig *assistConfig = [self getLiveAssistConfigFromSettings];
    assistConfig.delegate = self;
    assistConfig.javascriptMethodName = @"auth.getAuthenticationToken";
    _assistView = [[LiveAssistView alloc] initWithAssistConfig:assistConfig];
    [self.view addSubview:_assistView];
    _assistView.hidden = YES;
}

-(AssistConfig*) getLiveAssistConfigFromSettings {
    NSString* accountId = [AppSettings accountIdentifier];
    LiveAssistChatStyle style = [AppSettings chatStyle];
    BOOL notifications = YES;
    return [AssistConfig assistConfigWithAccountId:[accountId intValue] sections:@[] chatStyle:style frame:self.view.frame notifications:notifications];
}

-(void) authoriseChatWithCallback : (AuthoriseCallback)  callback {
    
    NSString *crm_guid = [AppSettings crm_guid];
    NSLog(@"crm_guiid: %@", crm_guid);
    
    NSDictionary *payload = @{@"sub" : crm_guid,
                              @"iss" : @"cafex.com",
                              @"iat" : @"1507896905859",
                              @"exp" : @"1507896970435"
                              };
    
    NSLog(@"payload: %@", payload);
    
//    NSString *auth_secret = [AppSettings auth_secret];
//
//    NSLog(@"secret %@", auth_secret);
//
//    id<JWTAlgorithm> algorithm = [JWTAlgorithmFactory algorithmByName:@"RS256"];
//
//     NSLog(@"algorithm %@", algorithm);
    
    NSString *algorithmName = @"RS256";
    NSLog(@"algorithmName: %@", algorithmName);

    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"certificate" ofType:@"p12"];
    NSData *privateKeySecretData = [NSData dataWithContentsOfFile:filePath];
    
    NSLog(@"privateKeySecretData: %@", privateKeySecretData);
    
    NSString *passphraseForPrivateKey = @"secret";
    
    JWTBuilder *builder = [JWTBuilder encodePayload:payload].secretData(privateKeySecretData).privateKeyCertificatePassphrase(passphraseForPrivateKey).algorithmName(algorithmName);
    NSString *token = builder.encode;
    
    // Deprecated
    //NSString *token = [JWTBuilder encodePayload:payload].secret(auth_secret).algorithm(algorithm).encode;
    
    NSLog(@"new token: %@", token);

    callback(token);
    
//    NSString *jwt = [AppSettings jwt];
//    NSLog(@"JWT: %@", jwt);
//    NSString *authString = jwt;
//    
//    callback(authString);

}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    [self addOnClickFor:[AppSettings identifier1] withSectionId:[AppSettings sectionForId1]];
    [self addOnClickFor:[AppSettings identifier2] withSectionId:[AppSettings sectionForId2]];
}

-(void) addOnClickFor : (NSString*) identfier withSectionId : (NSString*) section {

    NSString* addOnClickJavascript = [NSString stringWithFormat:@"addclickHandler('%@','%@');",identfier,section];
    [webView evaluateJavaScript:addOnClickJavascript completionHandler:nil];
}



- (IBAction)openChatWindow:(id)sender {
    _assistView.hidden = !_assistView.hidden;
}

- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    NSLog(@"Received event %@", message.body);
    [_assistView setSections:@[message.body]];
    [_assistView reload];
    _assistView.hidden = NO;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*) loadJavaScriptFromFileName : (NSString*) fileName {
    NSBundle *bundle = [NSBundle bundleForClass:[ViewController class]];
    NSString *txtFilePath = [bundle pathForResource:fileName ofType:@"js"];
    return [NSString stringWithContentsOfFile:txtFilePath encoding:
            NSUTF8StringEncoding error:NULL];
}

- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential *))completionHandler {
    
    NSURLCredential * credential = [[NSURLCredential alloc] initWithTrust:[challenge protectionSpace].serverTrust];
    completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
}

- (IBAction)back:(id)sender {
    if ([webView canGoBack]) {
        [webView goBack];
    }
    
}

- (IBAction)forward:(id)sender {
    if ([webView canGoForward]) {
        [webView goForward];
    }
}

- (IBAction)refresh:(id)sender {
    [webView reload];
}

    

@end
