#import <Foundation/Foundation.h>
#import <LiveAssist/LiveAssist.h>

/**
    Facade onto app settings stored in NSUserDefaults.
*/
@interface AppSettings : NSObject

+ (void)registerDefaultsFromSettingsBundle;
+ (NSString*) accountIdentifier;
+ (LiveAssistChatStyle) chatStyle;
+ (NSString*) webSite;
+ (NSString*) jwt;
+ (NSString*) identifier1;
+ (NSString*) sectionForId1;
+ (NSString*) identifier2;
+ (NSString*) sectionForId2;

+ (NSString*) auth_secret;
+ (NSString*) crm_guid;

@end
