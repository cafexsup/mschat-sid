//
//  AppDelegate.h
//  mschat-finance-sample
//
//  Created by Phillip Killen on 13/07/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppSettings.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

