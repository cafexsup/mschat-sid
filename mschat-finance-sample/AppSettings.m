#import "AppSettings.h"

@implementation AppSettings

+ (void)registerDefaultsFromSettingsBundle {
    // this function writes default settings as settings
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString * web_site_preference = [standardUserDefaults objectForKey:@"account_identifer"];
    if (!web_site_preference) {
        NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
        if(!settingsBundle) {
            NSLog(@"Could not find Settings.bundle");
            return;
        }
        
        NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
        NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
        
        NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
        for(NSDictionary *prefSpecification in preferences) {
            NSString *key = [prefSpecification objectForKey:@"Key"];
            if(key) {
                [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
                NSLog(@"writing as default %@ to the key %@",[prefSpecification objectForKey:@"DefaultValue"],key);
            }
        }
        
        [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    }
}

+ (NSString*) accountIdentifier {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"account_identifer"];
}

+ (LiveAssistChatStyle) chatStyle {
    NSInteger settingsValue = [[[NSUserDefaults standardUserDefaults] objectForKey:@"chat_style"] integerValue];
    switch (settingsValue) {
        case 0: return LiveAssistChatStyleAuto; break;
        case 1: return LiveAssistChatStyleFullScreen; break;
        case 2: return LiveAssistChatStylePopup; break;
        default: return LiveAssistChatStyleAuto; break;
    }
}

+ (NSString*) webSite {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"web_site"];
}

+ (NSString*) jwt {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"jwt"];
}

+ (NSString*) auth_secret {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"auth_secret"];
}

+ (NSString*) crm_guid {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"crm_guid"];
}

+ (NSString*) identifier1 {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"identifier1"];
}

+ (NSString*) sectionForId1 {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"id1_sections"];
}


+ (NSString*) identifier2 {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"identifier2"];
}

+ (NSString*) sectionForId2 {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"id2_sections"];
}



@end
