var elementToAppearWaitTimeInMS = 750;
var minimized = false;
var sendRectangleonMutation = null;


function addMutationObserver() {
    
    var observer = new MutationObserver(function (mutations) {
                                        
                                        for (var i = 0; i < mutations.length; ++i) {
                                        for (var j = 0; j < mutations[i].addedNodes.length; ++j) {
                                        var id = mutations[i].addedNodes[j].id;
                                        if (typeof id != 'undefined') {
                                        console.log(id);
                                        if (id.includes('lp_line') == true){
                                        
                                        var isWindowMinimised = false;
                                        var minWindow = document.getElementsByClassName("lp_minimized")[0];
                                        var display = window.getComputedStyle(minWindow, null).getPropertyValue('display');
                                        if(display === 'block') {isWindowMinimised = true;}
                                        
                                        var line = mutations[i].addedNodes[j];
                                        var chatLine = {
                                        text : line.getElementsByClassName("lp_title_text")[0].textContent,
                                        sender : line.getElementsByClassName("lp_sender")[0].textContent,
                                        minimized : isWindowMinimised
                                        };
                                        sendMessage(JSON.stringify(chatLine),"chatLine");
                                        break;
                                        
                                        }
                                        if (id.includes('LPMcontainer') == true)
                                            sendMessage("TAG IS ON THE PAGE..", "tagActive");
                                            sendRectangleUpdate();
                                        
                                        
                                        }
                                        }
                                        }

                                        modifyChatWindowButtonSizes();
                                        removeButton('lp_send_button');
                                        removeButton('lp_popout');
                                        removeButton('lp_actions_button');
                                        if(sendRectangleonMutation != null){
                                        clearTimeout(sendRectangleonMutation);
                                        }
                                        sendRectangleonMutation = setTimeout(function(){ getLivePersonRectangles(); }, 2000);
                                        
                                        });
    var observerConfig = {
    childList: true,
    subtree: true,
    attributes: true,
    characterData: false
    };
    
    var targetNode = document.body;
    observer.observe(targetNode, observerConfig);
    
}

function addLivePerson(accountId) {
    
    logMessage('Account ' + accountId + 'added to page');
    
    window.lpTag = window.lpTag || {};
    if (typeof window.lpTag._tagCount === 'undefined') {
        window.lpTag = {
        site: accountId,
        section: lpTag.section || '',
        autoStart: lpTag.autoStart === false ? false : true,
        ovr: lpTag.ovr || {},
        _v: '1.6.0',
        _tagCount: 1,
        protocol: 'https:',
        events: {
        bind: function (app, ev, fn) {
            lpTag.defer(function () {
                        lpTag.events.bind(app, ev, fn);
                        }, 0);
        }, trigger: function (app, ev, json) {
            lpTag.defer(function () {
                        lpTag.events.trigger(app, ev, json);
                        }, 1);
        }
        },
        defer: function (fn, fnType) {
            if (fnType == 0) {
                this._defB = this._defB || [];
                this._defB.push(fn);
            } else if (fnType == 1) {
                this._defT = this._defT || [];
                this._defT.push(fn);
            } else {
                this._defL = this._defL || [];
                this._defL.push(fn);
            }
        },
        load: function (src, chr, id) {
            var t = this;
            setTimeout(function () {
                       t._load(src, chr, id);
                       }, 0);
        },
        _load: function (src, chr, id) {
            var url = src;
            if (!src) {
                url = this.protocol + '//' + ((this.ovr && this.ovr.domain) ? this.ovr.domain : 'lptag.liveperson.net') + '/tag/tag.js?site=' + this.site;
            }
            var s = document.createElement('script');
            s.setAttribute('charset', chr ? chr : 'UTF-8');
            if (id) {
                s.setAttribute('id', id);
            }
            s.setAttribute('src', url);
            document.getElementsByTagName('head').item(0).appendChild(s);
        },
        init: function () {
            this._timing = this._timing || {};
            this._timing.start = (new Date()).getTime();
            var that = this;
            if (window.attachEvent) {
                window.attachEvent('onload', function () {
                                   that._domReady('domReady');
                                   });
            } else {
                window.addEventListener('DOMContentLoaded', function () {
                                        that._domReady('contReady');
                                        }, false);
                window.addEventListener('load', function () {
                                        that._domReady('domReady');
                                        }, false);
            }
            if (typeof (window._lptStop) == 'undefined') {
                this.load();
            }
        },
        start: function () {
            this.autoStart = true;
        },
        _domReady: function (n) {
            if (!this.isDom) {
                this.isDom = true;
                this.events.trigger('LPT', 'DOM_READY', {t: n});
            }
            this._timing[n] = (new Date()).getTime();
        },
        vars: lpTag.vars || [],
        dbs: lpTag.dbs || [],
        ctn: lpTag.ctn || [],
        sdes: lpTag.sdes || [],
        ev: lpTag.ev || []
        };
        lpTag.init();
    } else {
        window.lpTag._tagCount += 1;
    }
    
    lpTag.events.bind({
                      eventName: 'cobrowseAccepted',
                      appName: '*',
                      func: function (event) {
                      var elements = document.querySelectorAll('[data-assist-a' + event.agentId + ']');
                      var data = JSON.parse(atob(elements[elements.length - 1].dataset['assistA' + event.agentId]));
                      //                      window.webkit.messageHandlers.assistView.postMessage(JSON.stringify(data));
                      window.setTimeout(function() {
                          sendMessage(JSON.stringify(data), "coBrowse");
                      }, 2000);
                                        
                      lpTag.events.trigger({
                          appName: "cobrowse",
                          eventName: "sessionStarted",
                          data: {}
                      });
                      },
                      async: true
                      });
    
    
    lpTag.events.bind({
                      eventName: 'maximized',
                      appName: '*',
                      func: function (event) {
                      sendRectangleUpdate();
                      
                      },
                      async: true
                      });
    
    lpTag.events.bind({
                      eventName: 'minimized',
                      appName: '*',
                      func: function (event) {

                      sendRectangleUpdate();
                      },
                      async: true
                      });
    
    
    lpTag.events.bind(
                      {
                      appName: '*',
                      eventName: 'state',
                      func: function (e) {
                      sendMessage(e, "state");
                      sendRectangleUpdate();

                      }
                      });
    
    lpTag.events.bind({
                      eventName: '*',
                      appName: '*',
                      func: function (event) {

                      console.log(event);
                      
                      
                      if (event.state == 'ended') {
                        sendMessage("Chat has ended..", "endSupport");
                      sendRectangleUpdate();

                      }
                      
                      
                      if (eventName.indexOf('init') !== -1) {
                      addCloseListener();
                      sendRectangleUpdate();

                      }
                      
                      
                      },
                      async: true
                      });
    
    
}

function addCloseListener() {
    
    var close = document.getElementsByClassName("lp_close");
    for (var i = 0; i < close.length; i++) {
        close[i].addEventListener('click', function () {
            sendRectangleUpdate();
        });
    }
}

function logMessage(message) {
    sendMessage(message, "log");
}

function getLivePersonRectangles() {
    var maximized = document.getElementsByClassName("lp_maximized")[0];
    var minimized = document.getElementsByClassName("lp_minimized")[0];
    
    var viewData = {
    innerWidth: window.innerWidth,
    innerHeight: window.innerHeight,
    rectangles: []
    };
    
    var livePersonButtons = document.getElementsByClassName("LPMcontainer");
    
    for (var i = 0; i != livePersonButtons.length; i++) {
        addElementRectangleToRectangleList2("LPMcontainer", viewData.rectangles, i);
    }
    
    addElementRectangleToRectangleList("lp_maximized", viewData.rectangles);
    addElementRectangleToRectangleList("lp_minimized", viewData.rectangles);
    
    var rectangles = JSON.stringify(viewData);
    sendMessage(rectangles, "rectangles");
}

function addElementRectangleToRectangleList(elementName, rectangleList) {
    var element = document.getElementsByClassName(elementName)[0];
    
    if (element) {
        var rect = element.getBoundingClientRect();
        rectangleList.push({
                           name: elementName,
                           left: rect.left,
                           right: rect.right,
                           top: rect.top,
                           bottom: rect.bottom,
                           width: rect.width,
                           height: rect.height
                           });
    }
}

function addElementRectangleToRectangleList2(elementName, rectangleList, index) {
    var element = document.getElementsByClassName(elementName)[index];
    
    if (element) {
        var rect = element.getBoundingClientRect();
        rectangleList.push({
                           name: elementName,
                           left: rect.left,
                           right: rect.right,
                           top: rect.top,
                           bottom: rect.bottom,
                           width: rect.width,
                           height: rect.height
                           });
    }
}

function setSection(section) {
    lpTag.section = section.split(',');
    logMessage(section);
    
} 

function modifyChatWindowButtonSizes() {
    
    var close = document.getElementsByClassName("lp_header-buttons-container");
    for (var j = 0; j != close.length; j++) {
        close[j].style.width = '150px';
        for (var i = 0; i != close[j].childNodes.length; i++) {
            
            if (close[j].childNodes[i].nodeType == 1) {
                close[j].childNodes[i].style.width = '75px';
                close[j].childNodes[i].childNodes[0].style.marginLeft = '25px'
            }
            
        }
    }
}

function removeButton(buttonClassName){
    var buttons = document.getElementsByClassName(buttonClassName);
    
    for (var j = 0; j != buttons.length; j++) {
        buttons[j].style.display = 'none';
    }
}

function openChatWindow(){
                      document.getElementsByClassName("lp_maximize")[0].click();
                      sendRectangleUpdate();
}

function setEngagementAttributes(engagementAttributes){
    lpTag.sdes = [];
    lpTag.sdes.push(JSON.parse(engagementAttributes));
    lpTag.taglets.lp_sdes.push(lpTag.sdes);
}

function sendRectangleUpdate() {

    window.setTimeout(function () {
                      getLivePersonRectangles();
                      }, elementToAppearWaitTimeInMS);
    
}


var messageSendingInterval = 250;

var myVar = setInterval(sendMessages, messageSendingInterval);
var messageStack = [];

function sendMessage(message, type) {
    var jsonMessage = {type: type, message: message};
    messageStack.push(jsonMessage);
}

function sendMessages() {
    var jsonMessage = messageStack.pop();
    if (typeof jsonMessage != 'undefined' && jsonMessage) {
        var jsonString = JSON.stringify(jsonMessage);
        console.log('New Message count' + messageStack.length +'Sending message ' + jsonString);
        window.location.href = "assist://" + jsonString;
    }
}
