var authCallback = null;

function authoriseChat(jws) {
    authCallback(jws);
}

function cancelAuthCallback() {
    authCallback(null, "Authorisation canceled");
}

function liveAssistAuthorise(callback) {
    authCallback = callback;
    console.log("Get Auth Data for Live Person!");
    sendMessage("oAuth", "oAuth");
}
