#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LiveAssistDelegate.h"
#import "AssistMask.h"

/**
    Options for the visual style of the Live Assist chat UI. Default is LiveAssistChatSizeAuto.
    
    When LiveAssistChatSizeAuto is selected, the chat UI will adopt LiveAssistChatSizeFullScreen
    for the compact-width size class, and LiveAssistChatSizePopup for regular-width.
*/
typedef NS_ENUM(NSInteger, LiveAssistChatStyle) {
    LiveAssistChatStyleAuto,
    LiveAssistChatStyleFullScreen,
    LiveAssistChatStylePopup
};


@interface AssistConfig : NSObject

@property NSString *url;
@property LiveAssistChatStyle chatStyle;
@property NSInteger accountId;
@property CGRect frame;
@property NSArray* sections;
@property (nonatomic,weak) id <LiveAssistDelegate> delegate;
@property (nonatomic,weak) NSString* javascriptMethodName;
@property AssistMask* mask;
@property BOOL notifications;

+(instancetype) assistConfigWithAccountId: (NSInteger) accountId
                                 sections: (NSArray*) sections
                                chatStyle: (LiveAssistChatStyle) style
                                    frame: (CGRect) frame
                            notifications: (BOOL) notifications;

@end
