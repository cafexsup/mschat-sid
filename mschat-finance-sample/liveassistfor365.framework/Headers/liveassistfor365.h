#import <UIKit/UIKit.h>

//! Project version number for liveassistfor365.
FOUNDATION_EXPORT double liveassistfor365VersionNumber;

//! Project version string for liveassistfor365.
FOUNDATION_EXPORT const unsigned char liveassistfor365VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <liveassistfor365/PublicHeader.h>
#import "LiveAssistView.h"
#import "AssistConfig.h"

