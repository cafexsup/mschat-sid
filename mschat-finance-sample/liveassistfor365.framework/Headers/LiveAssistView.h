#import <UIKit/UIKit.h>
#import "AssistConfig.h"

@interface LiveAssistView : UIWebView<UIWebViewDelegate>

- (instancetype)initWithAssistConfig : (AssistConfig*) assistConfig;
-(void) setSections : (NSArray*) sections;
-(void) setEngagementAttributesViaJson : (NSString*) json;

@end
