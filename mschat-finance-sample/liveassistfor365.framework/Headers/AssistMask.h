#import <Foundation/Foundation.h>
@import UIKit;

/**
 The AssistMask class is used to provide masking information to LiveAssist
 */
@interface AssistMask : NSObject

/**
 Set of view Tags to be masked.
 */
@property NSSet *set;

/**
 Colour of mask
 */
@property UIColor *color;

/**
 * Creates a AssistMask Object which is used by AssistConfig to mask Native Content
 * @author CafeX
 *
 * @param set Set of view Tags to be masked.
 * @param color Color of the mask
 */
+(instancetype) withTagSet : (NSSet*) set andColor : (UIColor*) color;


-(void) addMaskToDictionary : (NSMutableDictionary *) dictionary;


@end
