#import <Foundation/Foundation.h>

@protocol LiveAssistDelegate <NSObject>

typedef void (^AuthoriseCallback)(NSString* authString);

@optional
-(void) authoriseChatWithCallback : (AuthoriseCallback)  callback;
@end
