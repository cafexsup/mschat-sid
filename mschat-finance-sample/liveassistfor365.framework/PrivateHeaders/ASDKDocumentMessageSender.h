#import "ASDKSharedDocument.h"

@protocol ASDKDocumentMessageSender <NSObject>
@required
- (void)sendDocumentFailedMessage:(ASDKSharedDocument *)sharedDoc;
- (void)sendDocumentSucceededMessage:(ASDKSharedDocument *)sharedDoc;
- (void)sendDocumentRejectedMessage:(ASDKSharedDocument *)sharedDoc;
- (void)sendDocumentAcceptedMessage:(ASDKSharedDocument *)sharedDoc;
- (void)sendDocumentClosedMessage:(ASDKSharedDocument *)sharedDoc initiator:(unsigned short)initiator;

@end