#import <Foundation/Foundation.h>

@protocol ASDKDocumentViewConstraints <NSObject>

- (float) topMargin;
- (float) bottomMargin;
- (float) leftMargin;
- (float) rightMargin;

@end
