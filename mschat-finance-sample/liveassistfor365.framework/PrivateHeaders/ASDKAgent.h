#import <Foundation/Foundation.h>

const static NSString *AssistAgentViewableKey = @"viewable";
const static NSString *AssistAgentInteractiveKey = @"interactive";
const static NSString *AssistDefaultPermission = @"default";
const static NSString *AssistAgentNameKey = @"name";

@interface ASDKAgent : NSObject

@property (atomic, retain) NSNumber *agentIdentifier;
@property (atomic, retain) NSDictionary *agentPermissions;
@property (atomic, assign) NSString *name;
@end
