#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger,DocType) {
#ifndef USE_ASDK_DOCUMENT_TYPES
    PDF=0,
    Image=1,
    Link=2,
    Unknown=3,
#endif
    ASDKPDF=0,
    ASDKImage=1,
    ASDKLink=2,
    ASDKUnknown=3
};

@class ASDKSharedDocument;
@protocol AssistSDKConsumerDocumentDelegate;

@protocol ASDKSharedDocumentDownloadDelegate <NSObject>

- (void)documentDidFinishDownloading:(ASDKSharedDocument *)sharedDocument;
- (void)documentFailedToDownload:(ASDKSharedDocument *)sharedDocument;

@end

/* A PDF file, image, or link to a web page pushed from the agent to the consumer. */
@interface ASDKSharedDocument : NSObject {
    NSMutableData *_receivedData;
}

@property (nonatomic, weak) id<ASDKSharedDocumentDownloadDelegate> downloadDelegate;

/*!
 * The id of a document shared by a consumer is always -1
 */
@property (nonatomic, retain) NSNumber *idNumber;
@property (nonatomic, retain) NSURL *url;
@property (nonatomic, assign) DocType docType;
@property (nonatomic, retain) NSString *mimeType;
@property (nonatomic, retain) NSString *characterEncoding;
@property (nonatomic, retain) NSData *contentData;
@property (nonatomic, assign) unsigned short errorCode;
@property (nonatomic, assign) NSInteger httpErrorCode;
@property (nonatomic, retain) NSString *errorDetails;
@property (nonatomic, assign) bool hasCloseLink;
@property (nonatomic, assign) bool consumerPush;
@property (nonatomic, assign)  id<AssistSDKConsumerDocumentDelegate> consumerShareDelegate;
@property (nonatomic, retain) NSString *metadata;
- (id)initWithUrl:(NSURL *)url link:(BOOL)link __attribute__((deprecated));
- (id)initWithUrl:(NSURL *)url link:(bool)link hasCloseLink:(bool)hasCloseLink;
- (id)initWithMimeType:(NSString *)mimeType;
- (id)initWithMimeType:(NSString *)mimeType andContent:(NSData *)content hasCloseLink:(bool) hasCloseLink;
- (BOOL)isLink;
- (void)determineDocTypeFromMimeType;
- (void)downloadWithDelegate:(id<ASDKSharedDocumentDownloadDelegate>)delegate;
- (void)prepareToReceiveDataChunks;
- (void)addDataChunk:(NSData *)data;
- (void)doneReceivingDataChunks;

- (void)close;

@end
