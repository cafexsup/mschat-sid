#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "ASDKSharedDocument.h"
#import "ASDKScreenShareRequestedDelegate.h"
#import "ASDKAgent.h"
#import "ASDKErrorCodes.h"
 
typedef NS_ENUM(NSInteger, AssistStatus) {
    AssistStatusInitialized,
    AssistStatusRunning,
    AssistStatusStopped
};

extern NSString *kASDKSVGPathKey;
extern NSString *kASDKSVGLayerKey;

extern NSString *kASDKSVGPathStrokeKey;
extern NSString *kASDKSVGPathStrokeWidthKey;
extern NSString *kASDKSVGPathStrokeOpacityKey;

/* The AssistSDK also supports the following Protocols. Its
 implementation is to forward the notification to each of
 any registered delegates
 */

@protocol AssistSDKAnnotationDelegate <NSObject>
@optional
- (void) assistSDKWillAddAnnotation:(NSNotification*)notification;
- (void) assistSDKDidAddAnnotation:(NSNotification*)notification;
- (void) assistSDKDidClearAnnotations:(NSNotification*)notification;
@end


@protocol AssistSDKDelegate <NSObject>
@optional
- (void) assistSDKDidEncounterError:(NSNotification*)notification;
- (void) cobrowseActiveDidChangeTo:(BOOL)active;
- (void) supportCallDidEnd;
@end

/*!
 * Who closed the document.
 */
typedef NS_ENUM(NSInteger, AssistSDKDocumentCloseInitiator) {
    AssitSDKDocumentClosedByUnknown,
    AssistSDKDocumentClosedByAgent,
    AssistSDKDocumentClosedByConsumer,
    AssistSDKDocumentClosedBySupportEnded
};

@protocol AssistSDKDocumentDelegate <NSObject>
@optional
/*!
 * Deprecated since 1.22.0 (5/02/16)
 * Start using onOpened:
 * This method will not be called if the delegate method onOpened: is implemented.
 */
- (void) assistSDKDidOpenDocument:(NSNotification*)notification __attribute__((deprecated("Use onOpened:")));
/*!
 * Deprecated since 1.22.0 (5/02/16)
 * Start using onError:reason:
 * This method will not be called if the delegate method onError:reason: is implemented.
 */
- (void) assistSDKUnableToOpenDocument:(NSNotification*)notification __attribute__((deprecated("Use onError:reason:")));
/*!
 * Deprecated since 1.22.0 (5/02/16)
 * Start using onClosed:by:
 * This method will not be called if the delegate method onClosed:by: is implemented.
 */
- (void) assistSDKDidCloseDocument:(NSNotification*)notification __attribute__((deprecated("Use onClosed:by:")));

/*!
 * Called if there was an issue opening the document.
 * @param document that could not be shared.
 */
- (void) onError:(ASDKSharedDocument *)document reason:(NSString *)reasonStr;
/*!
 * Called if the document was closed.
 * @param document the document that was closed.
 * @param whom who closed the document.
 */
- (void) onClosed:(ASDKSharedDocument *)document by:(AssistSDKDocumentCloseInitiator) whom;

/*!
 * Called if the document was opened after being pushed by an agent.
 * @param document the document that was opened.
 */
- (void) onOpened:(ASDKSharedDocument *)document;
@end

/*!
 * Define a listener to be used when a consumer shares a document.
 */
@protocol AssistSDKConsumerDocumentDelegate <NSObject>
@optional
/*!
 * Called if there was an issue opening the document.
 * @param document document that could not be shared.
 * @param reasonStr human readable error message.
 */
- (void) onError:(ASDKSharedDocument *)document reason:(NSString *)reasonStr;
/*!
 * Called if the document was closed.
 * @param document the document that was closed.
 * @param whom who closed the document.
 */
- (void) onClosed:(ASDKSharedDocument *)document by:(AssistSDKDocumentCloseInitiator) whom;
@end

@interface AssistSDK : UIViewController <
    UIImagePickerControllerDelegate,
    UINavigationControllerDelegate,
    AssistSDKAnnotationDelegate>

+ (instancetype)sharedInstance;

/*!
 * Starts an Assist SDK support session. Unlike the generic "startSupport" method, this call does not set any of the optional parameters.
 * @param server The Assist server to use for the session. This can be just the name (or IP address) of the server or a URL
 * without a path (The URL is just used to specify the protocol and port. An example is: https://demoserver.test.com:8443 ).
 * @param target The destination of the call. This could be an agent or queue name. It can also be a full sip URL.
 */
+ (AssistSDK*) startSupport: (NSString*) server destination: (NSString*) target;

/*!
 * Starts an AssistSDK support session.
 * @param server The Assist Server to use for this session. This can be just the name (or IP address) of the server or a URL
 * without a path (The URL is just used to specify the protocol and port. An example is: https://demoserver.test.com:8443 ).
 * @param config A NSDictionary used to specify required and optional parameters to be used by the Assist SDK.
 * Currently, these parameters are (for full details please refer to the developer guide):
 * - destination: The destination of the call. This could be an agent or queue name. It can also be a full sip URL).
 * - hidingTags: NSSet of tag numbers used to identify which UI elements should be obscured with a black rectangle on the agent side when screen
 *   sharing.
 * - maskingTags: NSSet of tag numbers used to identify which UI elements should be masked on the agent side when screen sharing.
 * - maskColor: UIColor The color to use when masking UI elements.
 * - correlationId: Used to map screensharing sessions that uses external audio/video.
 * - uui: A user to user header value to include in outbound SIP message for external correlation purposes
 * - acceptSelfSignedCerts: Indicates if we should accept self-signed security certificates or not. This takes the @NO or @YES objects. Absence of this
 *    attribute will automatically lead to the refusal of self-signed certificates.
 * - timeout: How long (approximatively) should we wait to establish the communication with the server before we give up in seconds. This argument should be expressed as a NSNumber, ideally of type float (for example it could be set with [NSNumber numberWithFloat:30.0]).
 * - sessionToken: a pre-created session ID that will be used to establish connections
 * - useCookies: NSNumber (@YES,@NO) - determine whether whether cookies set up to be sent to the Live Assist server should be sent on the web socket
 *   connection. Default is @NO.
 * - videoMode: Sets the video mode of a call (full, agentOnly or none). Default is full.
 * - keepAnnotationsOnChange: NSNumber (@YES,@NO)- Whether annotations are retained when the content behind them changes. Default is @NO.
 * - addSharedDocCloseLink: NSNumber (@YES, @NO) - Whether a close link should be added to a shared document. Default is @YES.
 * - screenShareRequestedDelegate: A delegate which conforms to the protocol ASDKScreenShareRequestedDelegate. Specifying this and conforming to this
 *   protocol allows an application to choose whether to accept or reject screen sharing however it sees fit.
 * - pushDelegate: A delegate which conforms to the protocol ASDKPushAuthorizationDelegate. Specifying this and conforming to this protocol allows
 *   an application to choose whether to accept or reject pushed content however it sees fit.
 */
+ (AssistSDK*) startSupport: (NSString*) server supportParameters: (NSDictionary*) config;

/*!
 * Ends the active Assist SDK support session if one is in progress. Otherwise has no affect.
 */
+ (void) endSupport;

/*!
 * The delegate is inspected to determine which, if any, Assist Delegate Protocols it supports.
 * If
*/

+ (BOOL) addDelegate:delegate;
+ (BOOL) removeDelegate:delegate;

/*!
 * Share a document located at a specified URL.
 * @param documentUrl the URL of the document to share.
 * @param closeLink whether to add a close button to the presented document.
 */
+ (void) shareDocumentUrl:(NSString *) documentUrl addCloseLink:(bool) closeLink __attribute__((deprecated("Use shareDocumentUrl:(NSString*) consumerShareDelegate:")));

/*!
 * Share a document located at a specified URL.
 * @param documentUrl the URL of the document to share.
 * @param closeLink whether to add a close button to the presented document.
 */
+ (void) shareDocumentNSUrl:(NSURL *) documentUrl addCloseLink:(bool) closeLink __attribute__((deprecated("Use shareDocumentUrl:(NSURL*) consumerShareDelegate:")));

/*!
 * Share a document with loaded content.
 * @param content the document content.
 * @param mimeType the mime type of the document content.
 * @param closeLink whether to add a close button to the presented document.
 */
+ (void) shareDocument:(NSData *) content mimeType:(NSString *)mimeType addCloseLink:(bool) closeLink __attribute__((deprecated("Use shareDocument:mimeType:delegate:")));

/*!
 * Share a document located at a specified URL.
 * @param documentUrl the URL of the document to share.
 * @param consumerShareDelegate delegate to use.
 * @return NSError non-nil if invoked prior to screen-share.
 */
+ (NSError *) shareDocumentUrl:(NSString *) documentUrl delegate:(id<AssistSDKConsumerDocumentDelegate>)consumerShareDelegate;

/*!
 * Share a document located at a specified URL.
 * @param documentUrl the URL of the document to share.
 * @param consumerShareDelegate delegate to use.
 * @return NSError non-nil if invoked prior to screen-share.
 */
+ (NSError *) shareDocumentNSUrl:(NSURL *) documentUrl delegate:(id<AssistSDKConsumerDocumentDelegate>)consumerShareDelegate;


/*!
 * Share a document with loaded content.
 * @param content the document content.
 * @param mimeType the mime type of the document content.
 * @param consumerShareDelegate delegate to use.
 * @return NSError non-nil if invoked prior to screen-share.
 */
+ (NSError *) shareDocument:(NSData *) content mimeType:(NSString *)mimeType delegate:(id<AssistSDKConsumerDocumentDelegate>)consumerShareDelegate;

- (AssistStatus)status;

/*!
 * Provide @"scheme", @"host" and @"port" details given the server string.
 * The above strings act as keys into the returned NSDictionary.
 */
+ (NSDictionary *) parseServerInfo:(NSString *)serverStr;

/*!
 * Set (or remove) a permission marker for a specified view.
 * @param permissionMarker if nil or empty, clear the marker for the specified view.
 * @param view the view to use - this method will do nothing if nil.
 */
+ (void) setPermission: (NSString *) permissionMarker forView:(UIView *) view;

/*!
 * Allow the specified agent to co-browse.
 * @param agent the agent in question.
 */
+ (void) allowCobrowseForAgent:(ASDKAgent *)agent;

/*!
 * Disallow the specified agent from co-browsing.
 * @param agent the agent in question.
 */
+ (void) disallowCobrowseForAgent:(ASDKAgent *)agent;

/*!
 * Allows an active co-browse to be paused.
 */
+ (void) pauseCobrowse;

/*!
 * Allows a paused co-browse to be resumed.
 */
+ (void) resumeCobrowse;

/*!
 * Allows the retrieval of support parameters
 */
+ (NSDictionary *)supportParameters;
@end






