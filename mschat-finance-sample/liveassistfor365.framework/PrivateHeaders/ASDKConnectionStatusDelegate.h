#import "ASDKConnector.h"

@protocol ASDKConnectionStatusDelegate <NSObject>

@required
- (void) onDisconnect:(NSError *) reason connector:(ASDKConnector *) connector;
- (void) onConnect;
- (void) onTerminated:(NSError *) reason;

@optional
- (void) willRetry:(float) inSeconds attempt:(int) attempt of:(int) maxAttempts connector:(ASDKConnector *) connector;
@end
