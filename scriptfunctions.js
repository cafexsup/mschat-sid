function addclickHandler(identifier,section) {
    console.log('adding the click handler for ' + identifier + 'with section' + section);
    var element = document.getElementById(identifier);
    element.onclick = function() {
        window.webkit.messageHandlers.observe.postMessage(section);
    }
}
